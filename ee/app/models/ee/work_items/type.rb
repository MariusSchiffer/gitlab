# frozen_string_literal: true

module EE
  module WorkItems
    module Type
      extend ActiveSupport::Concern
      extend ::Gitlab::Utils::Override
      include ::Gitlab::Utils::StrongMemoize

      LICENSED_WIDGETS = {
        iterations: ::WorkItems::Widgets::Iteration,
        issue_weights: ::WorkItems::Widgets::Weight,
        requirements: [
          ::WorkItems::Widgets::Status,
          ::WorkItems::Widgets::RequirementLegacy,
          ::WorkItems::Widgets::TestReports
        ],
        issuable_health_status: ::WorkItems::Widgets::HealthStatus,
        okrs: ::WorkItems::Widgets::Progress,
        epic_colors: ::WorkItems::Widgets::Color
      }.freeze

      LICENSED_TYPES = { epic: :epics, objective: :okrs, key_result: :okrs, requirement: :requirements }.freeze

      class_methods do
        extend ::Gitlab::Utils::Override

        override :allowed_group_level_types
        def allowed_group_level_types(resource_parent)
          allowed_types = super

          if ::Feature.enabled?(:work_item_epics, resource_parent.root_ancestor, type: :beta) &&
              resource_parent.licensed_feature_available?(:epics)
            allowed_types << 'epic'
          end

          allowed_types
        end
      end

      override :widgets
      def widgets(resource_parent)
        strong_memoize_with(:widgets, resource_parent) do
          unlicensed_classes = unlicensed_widget_classes(resource_parent)

          if epic? && !resource_parent.try(:work_items_beta_feature_flag_enabled?)
            unlicensed_classes << ::WorkItems::Widgets::Assignees
          end

          super.reject { |widget_def| unlicensed_classes.include?(widget_def.widget_class) }
        end
      end

      private

      def unlicensed_widget_classes(resource_parent)
        LICENSED_WIDGETS.flat_map do |licensed_feature, widget_class|
          widget_class unless resource_parent.licensed_feature_available?(licensed_feature)
        end.compact
      end

      override :supported_conversion_base_types
      def supported_conversion_base_types(resource_parent)
        ee_base_types = LICENSED_TYPES.flat_map do |type, licensed_feature|
          type.to_s if resource_parent.licensed_feature_available?(licensed_feature.to_sym)
        end.compact

        unless ::Feature.enabled?(:work_item_epics, resource_parent.root_ancestor, type: :beta)
          ee_base_types -= %w[epic]
        end

        project = if resource_parent.is_a?(Project)
                    resource_parent
                  elsif resource_parent.respond_to?(:project)
                    resource_parent.project.presence
                  end

        ee_base_types -= %w[objective key_result] unless project && ::Feature.enabled?(:okrs_mvc, project)

        super + ee_base_types
      end
    end
  end
end
